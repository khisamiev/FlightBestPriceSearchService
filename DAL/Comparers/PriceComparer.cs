﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Comparers
{
    /// <summary>
    /// Компаратор цен.
    /// </summary>
    public class PriceComparer : IComparer<Price>
    {
        public int Compare(Price x1, Price x2)
        {
            var convertedPrice1 = x1 != null && x1.Destination != null &&
                            x1.Destination.Country != null && x1.Destination.Country.BaseCurrency != null ?
                            x1.Value * x1.Destination.Country.BaseCurrency.ExchangeRate : default(decimal);
            var convertedPrice2 = x2 != null && x2.Destination != null &&
                            x2.Destination.Country != null && x2.Destination.Country.BaseCurrency != null ?
                            x2.Value * x2.Destination.Country.BaseCurrency.ExchangeRate : default(decimal);

            if (convertedPrice1 == convertedPrice2)
                return 0;
            if (convertedPrice1 > convertedPrice2)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }
}
