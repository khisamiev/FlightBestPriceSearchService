﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;

namespace DAL.DbInit
{
    /// <summary>
    /// Инициализатор контекста базы данных.
    /// </summary>
    public class PriceDatabaseInitializer : CreateDatabaseIfNotExists<BestFlightPriceContext>
    {
        public PriceDatabaseInitializer(IBestFlightPriceUnitOfWork context)
        {
            //Task.Run(() => InitializeIdentityForEF(context));
        }

        protected override void Seed(BestFlightPriceContext context)
        {
            base.Seed(context);
            //InitializeIdentityForEF(context);
        }

        /// <summary>
        /// Инициализирует начальные данные в БД.
        /// </summary>
        public async Task InitializeIdentityForEF(IBestFlightPriceUnitOfWork context)
        {
            var random = new Random();
            /*
            #region Currency
            //-------------------------------------------------------------------

            var rub = context.Currencies.Insert(new Currency() { Name = "RUB", ExchangeRate = 0.1M, Annotation = "Рубль" });
            var eur = context.Currencies.Insert(new Currency() { Name = "EUR", ExchangeRate = 1.3M, Annotation = "Евро" });
            var usd = context.Currencies.Insert(new Currency() { Name = "USD", ExchangeRate = 1.0M, Annotation = "Доллар" });
            context.Commit();

            //-------------------------------------------------------------------
            #endregion Currency

            #region Country
            //-------------------------------------------------------------------

            var countryCount = random.Next(10, 50);

            for (int i = 0; i < countryCount; i = i + 3)
            {
                context.Countries.Insert(new Country()
                {
                    Name = "Страна_" + (i).ToString(),
                    BaseCurrencyId = rub.Id
                });
                context.Countries.Insert(new Country()
                {
                    Name = "Страна_" + (i + 1).ToString(),
                    BaseCurrencyId = eur.Id
                });
                context.Countries.Insert(new Country()
                {
                    Name = "Страна_" + (i + 2).ToString(),
                    BaseCurrencyId = usd.Id
                });
            }
            context.Commit();
            //-------------------------------------------------------------------
            #endregion Country

            #region Cities
            //-------------------------------------------------------------------

            var countries = await context.Countries.GetAll();

            for (int i = 0; i < 4000; i++)
            {
                var lang = (decimal)random.NextDouble();
                var lat = (decimal)random.NextDouble();
                var countryId = countries[random.Next(0, countries.Count)];

                context.Cities.Insert(new City()
                {
                    Name = "Город_" + i.ToString(),
                    Latitude = lat,
                    Longitude = lang,
                    CountryId = countryId.Id
                });
            }
            context.Commit();
            //-------------------------------------------------------------------
            #endregion Cities
            */
            context.Commit();
            #region Prices
            //-------------------------------------------------------------------

            var cities = await context.Cities.GetAll();

            DateTime minDate = DateTime.UtcNow;
            DateTime maxDate = minDate.AddYears(1);

            foreach (var item in cities)
            {
                var priceCount = random.Next(250, 400);

                for (int i = 0; i < priceCount; i++)
                {
                    var sum = random.Next(100, 10000);
                    var dateFrom = DateHelper.GetRandomDate(minDate, maxDate);
                    var dateReturn = DateHelper.GetRandomDate(dateFrom, maxDate);

                    var destId = (Int16)random.Next(0, cities.Count());

                    if (destId == item.Id)
                        destId = (Int16)((destId + 1) % cities.Count());

                    context.Prices.Insert(new Price()
                    {
                        Value = sum,
                        DepartDate = dateFrom,
                        ReturnDate = dateReturn,
                        OriginId = item.Id,
                        DestinationId = destId
                    });
                    try
                    {
                        context.Commit();
                    }
                    catch (Exception) { }
                }
            }
            context.Commit();
            //-------------------------------------------------------------------
            #endregion Prices

        }
    }
}
