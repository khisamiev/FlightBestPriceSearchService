﻿using DAL.Base.Repositories;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Cities
{
    /// <summary>
    /// Интерфейс репозитория городов.
    /// </summary>
    public interface ICityRepository : IRepository<City, Int16>
    {
    }
}
