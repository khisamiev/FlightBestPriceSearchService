﻿using DAL.Base.Repositories;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Cities
{
    public sealed class CityRepository: BaseRepository<City,Int16>, ICityRepository
    {
        public CityRepository(DbContext context) :
            base(context)
        {
            
        }

        public async override Task<City> Find(Int16 id)
        {
            return await dbSet.FindAsync(id);
        }

        public override void Delete(Int16 id)
        {
            var entity = dbSet.Find(id);
            if (entity != null)
                dbSet.Remove(entity);
        }
    }
}
