﻿using DAL.Base.Repositories;
using DAL.Comparers;
using DAL.Contracts.Prices;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Prices
{
    public sealed class PriceRepository : BaseRepository<Price, PriceCompositeKey>, IPriceRepository
    {
        public PriceRepository(DbContext context) :
            base(context)
        {
            
        }
        public async override Task<Price> Find(PriceCompositeKey id)
        {
            return await dbSet.FindAsync(id.OriginId,id.DestinationId,id.DepartDate,id.ReturnDate);
        }

        public override void Delete(PriceCompositeKey id)
        {
            var entity = dbSet.Find(id.OriginId, id.DestinationId, id.DepartDate, id.ReturnDate);
            if (entity != null)
                dbSet.Remove(entity);
        }

        public async Task<IEnumerable<Price>> Get(IPriceFilterContract filter) {
            var skipValue = (filter.Page - 1) * filter.PageSize;
            var takeValue = filter.PageSize;
            IComparer<Price> comparer = new PriceComparer();

            return await dbSet
                                  .Where(filter.OriginCityExpression)
                                  .Where(filter.DateRangeExpression)
                                  .OrderBy(x => x.Value)
                                  .Skip(skipValue)
                                  .Take(takeValue)
                                  .AsQueryable()
                                  .ToListAsync();
        }

        public async Task<long> GetCount(IPriceFilterContract filter) {
            return await dbSet.Where(filter.OriginCityExpression).Where(filter.DateRangeExpression).CountAsync();
        }
    }
}
