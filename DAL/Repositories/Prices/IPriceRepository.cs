﻿using DAL.Base.Repositories;
using DAL.Contracts.Prices;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Prices
{
    /// <summary>
    /// Интерфейс репозитория цен.
    /// </summary>
    public interface IPriceRepository : IRepository<Price, PriceCompositeKey>
    {
        /// <summary>
        /// Возвращает цены по заданному фильтру.
        /// </summary>
        /// <param name="filter">Фильтр.</param>
        /// <returns></returns>
        Task<IEnumerable<Price>> Get(IPriceFilterContract filter);
        /// <summary>
        /// Возвращает количество цен по заданному фильтру.
        /// </summary>
        /// <param name="filter">Фильтр.</param>
        /// <returns></returns>
        Task<long> GetCount(IPriceFilterContract filter);
    }
}
