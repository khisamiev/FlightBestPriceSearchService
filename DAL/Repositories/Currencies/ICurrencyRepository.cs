﻿using DAL.Base.Repositories;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Currencies
{
    /// <summary>
    /// Интерфейс репозитория валют.
    /// </summary>
    public interface ICurrencyRepository : IRepository<Currency, Int16>
    {
    }
}
