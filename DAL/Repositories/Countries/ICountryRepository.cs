﻿using DAL.Base.Repositories;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Countries
{
    /// <summary>
    /// Интерфейс репозитория стран.
    /// </summary>
    public interface ICountryRepository : IRepository<Country, Int16>
    {
    }
}