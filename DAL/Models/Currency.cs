﻿using DAL.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    /// <summary>
    /// Валюта.
    /// </summary>
    [Table("Currencies")]
    [Serializable]
    public class Currency : IBaseEntity
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        [Key]
        public Int16 Id { get; set; }
        /// <summary>
        /// Имя.
        /// </summary>
        public String Name { get; set; }
        /// <summary>
        /// Описание.
        /// </summary>
        public String Annotation { get; set; }
        /// <summary>
        /// Курс валюты.
        /// </summary>
        public Decimal ExchangeRate { get; set; }
        /// <summary>
        /// Список стран.
        /// </summary>
        public virtual List<Country> Countries { get; set; }
    }
}
