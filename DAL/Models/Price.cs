﻿using DAL.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    /// <summary>
    /// Цена.
    /// </summary>
    [Table("Prices")]
    [Serializable]
    public class Price : IBaseEntity
    {
        /// <summary>
        /// Город вылета.
        /// </summary>
        [Key, Column(Order = 1)]
        [Required]
        public Int16 OriginId { get; set; }
        /// <summary>
        /// Город назначения.
        /// </summary>
        [Key, Column(Order = 2)]
        [Required]
        public Int16 DestinationId { get; set; }
        /// <summary>
        /// Дата вылета.
        /// </summary>
        [Key, Column(Order = 3)]
        [Required]
        public DateTime DepartDate { get; set; }
        /// <summary>
        /// Дата обратного вылета.
        /// </summary>
        [Key, Column(Order = 4)]
        [Required]
        public DateTime ReturnDate { get; set; }
        /// <summary>
        /// Цена.
        /// </summary>
        public decimal Value { get; set; }
        /// <summary>
        /// Город вылета.
        /// </summary>
        public virtual City Origin { get; set; }
        /// <summary>
        /// Город назначения.
        /// </summary>
        public virtual City Destination { get; set; }
    }
}
