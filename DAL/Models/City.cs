﻿using DAL.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    /// <summary>
    /// Город.
    /// </summary>
    [Table("Cities")]
    [Serializable]
    public class City : IBaseEntity
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        [Key]
        public Int16 Id { get; set; }
        /// <summary>
        /// Имя.
        /// </summary>
        public String Name { get; set; }
        /// <summary>
        /// Широта.
        /// </summary>
        public Decimal Latitude { get; set; }
        /// <summary>
        /// Долгота.
        /// </summary>
        public Decimal Longitude { get; set; }
        /// <summary>
        /// Идентификатор страны.
        /// </summary>
        public Int16 CountryId { get; set; }
        /// <summary>
        /// Страна.
        /// </summary>
        public virtual Country Country { get; set; }
    }
}
