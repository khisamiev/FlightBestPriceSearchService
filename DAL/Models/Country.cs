﻿using DAL.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    /// <summary>
    /// Страна.
    /// </summary>
    [Table("Countries")]
    [Serializable]
    public class Country : IBaseEntity
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        [Key]
        public Int16 Id { get; set; }
        /// <summary>
        /// Имя.
        /// </summary>
        public String Name { get; set; }
        /// <summary>
        /// Идентификатор валюты.
        /// </summary>
        public Int16 BaseCurrencyId { get; set; }
        /// <summary>
        /// Валюта.
        /// </summary>
        public virtual Currency BaseCurrency { get; set; }
        /// <summary>
        /// Список городов.
        /// </summary>
        public virtual List<City> Cities { get; set; }
    }
}
