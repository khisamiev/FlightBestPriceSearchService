namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v10init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(),
                        Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CountryId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.CountryId)
                .Index(t => t.CountryId);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(),
                        BaseCurrencyId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Currencies", t => t.BaseCurrencyId)
                .Index(t => t.BaseCurrencyId);
            
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(),
                        Annotation = c.String(),
                        ExchangeRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Prices",
                c => new
                    {
                        OriginId = c.Short(nullable: false),
                        DestinationId = c.Short(nullable: false),
                        DepartDate = c.DateTime(nullable: false),
                        ReturnDate = c.DateTime(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => new { t.OriginId, t.DestinationId, t.DepartDate, t.ReturnDate })
                .ForeignKey("dbo.Cities", t => t.DestinationId)
                .ForeignKey("dbo.Cities", t => t.OriginId)
                .Index(t => t.OriginId)
                .Index(t => t.DestinationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Prices", "OriginId", "dbo.Cities");
            DropForeignKey("dbo.Prices", "DestinationId", "dbo.Cities");
            DropForeignKey("dbo.Cities", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.Countries", "BaseCurrencyId", "dbo.Currencies");
            DropIndex("dbo.Prices", new[] { "DestinationId" });
            DropIndex("dbo.Prices", new[] { "OriginId" });
            DropIndex("dbo.Countries", new[] { "BaseCurrencyId" });
            DropIndex("dbo.Cities", new[] { "CountryId" });
            DropTable("dbo.Prices");
            DropTable("dbo.Currencies");
            DropTable("dbo.Countries");
            DropTable("dbo.Cities");
        }
    }
}
