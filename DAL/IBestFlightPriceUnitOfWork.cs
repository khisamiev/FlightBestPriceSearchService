﻿using DAL.Base;
using DAL.Repositories.Cities;
using DAL.Repositories.Countries;
using DAL.Repositories.Currencies;
using DAL.Repositories.Prices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    /// <summary>
    /// Интерфейс доступа к данным BestFlightPriceContext.
    /// </summary>
    public interface IBestFlightPriceUnitOfWork : IBaseUnitOfWork
    {
        /// <summary>
        /// Репозиторий городов.
        /// </summary>
        ICityRepository Cities { get; }
        /// <summary>
        /// Репозиторий стран.
        /// </summary>
        ICountryRepository Countries { get; }
        /// <summary>
        /// Репозиторий валют.
        /// </summary>
        ICurrencyRepository Currencies { get; }
        /// <summary>
        /// Репозиторий цен.
        /// </summary>
        IPriceRepository Prices { get; }  
    }
}
