﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contracts.Filter
{
    /// <summary>
    /// Базовый фильтр.
    /// </summary>
    public class BaseFilterContract : IBaseFilterContract
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}
