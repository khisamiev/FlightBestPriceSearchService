﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contracts.Filter
{
    /// <summary>
    /// Интерфейс базового фильтра.
    /// </summary>
    public interface IBaseFilterContract
    {
        /// <summary>
        /// Страница.
        /// </summary>
        int Page { get; set; }
        /// <summary>
        /// Размер страницы.
        /// </summary>
        int PageSize { get; set; }
    }
}
