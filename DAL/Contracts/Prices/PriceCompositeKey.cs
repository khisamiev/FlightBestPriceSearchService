﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contracts.Prices
{
    /// <summary>
    /// Контракт составного ключа цены.
    /// </summary>
    public struct PriceCompositeKey
    {
        /// <summary>
        /// Город вылета.
        /// </summary>
        public Int16 OriginId { get; set; }
        /// <summary>
        /// Город назначения.
        /// </summary>
        public Int16 DestinationId { get; set; }
        /// <summary>
        /// Дата вылета.
        /// </summary>
        public DateTime DepartDate { get; set; }
        /// <summary>
        /// Дата обратного вылета.
        /// </summary>
        public DateTime ReturnDate { get; set; }
    }
}
