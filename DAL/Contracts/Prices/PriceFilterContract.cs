﻿using DAL.Contracts.Filter;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contracts.Prices
{
    public class PriceFilterContract : BaseFilterContract, IPriceFilterContract
    {
        public Expression<Func<Price, bool>> OriginCityExpression { get; set; }
        public Expression<Func<Price, bool>> DateRangeExpression { get; set; }
    }
}
