﻿using DAL.Contracts.Filter;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contracts.Prices
{
    /// <summary>
    /// Интерфейс фильтра цен.
    /// </summary>
    public interface IPriceFilterContract : IBaseFilterContract
    {
        /// <summary>
        /// Привязка к пункту отправления.
        /// </summary>
        Expression<Func<Price, bool>> OriginCityExpression { get; set; }
        /// <summary>
        /// Привязка к временному промежутку.
        /// </summary>
        Expression<Func<Price, bool>> DateRangeExpression { get; set; }
    }
}
