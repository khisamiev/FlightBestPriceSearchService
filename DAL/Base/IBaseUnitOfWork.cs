﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Base
{
    /// <summary>
    /// Интерфейс доступа к данным HomeBanking.
    /// </summary>
    public interface IBaseUnitOfWork
    {
        /// <summary>
        /// Контекст БД.
        /// </summary>
        BestFlightPriceContext Сontext { get; }
        /// <summary>
        /// Возвращает репозиторий сущности.
        /// </summary>
        /// <typeparam name="T">Тип сущности.</typeparam>
        /// <returns></returns>
        T GetRepository<T>() where T : class;
        /// <summary>
        /// Сохранение.
        /// </summary>
        void Commit();
        /// <summary>
        /// Отмена.
        /// </summary>
        void Rollback();
        /// <summary>
        /// Уничтожение.
        /// </summary>
        void Dispose();
    }
}
