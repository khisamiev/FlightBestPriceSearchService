﻿using DAL.Repositories.Cities;
using DAL.Repositories.Countries;
using DAL.Repositories.Currencies;
using DAL.Repositories.Prices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Base
{
    /// <summary>
    /// Объект доступа к данным HomeBanking.
    /// </summary>
    public class BaseUnitOfWork : IBaseUnitOfWork
    {
        public BestFlightPriceContext Сontext { get { return _context; } }

        public BaseUnitOfWork() : this(new BestFlightPriceContext()) { }

        public BaseUnitOfWork(BestFlightPriceContext context)
        {
            _context = context;
            _context.Configuration.AutoDetectChangesEnabled = true;
            _context.Configuration.LazyLoadingEnabled = true;
            _context.Configuration.ProxyCreationEnabled = true;
            _context.Configuration.ValidateOnSaveEnabled = false;

            _repositoryFactories = new Dictionary<Type, Func<BestFlightPriceContext, object>>() {
                
                    {typeof(ICityRepository), x => new CityRepository(x) },
                     {typeof(ICountryRepository), x => new CountryRepository(x) },
                      {typeof(ICurrencyRepository), x => new CurrencyRepository(x) },
                       {typeof(IPriceRepository), x => new PriceRepository(x) }
                };
            _repositories = new Dictionary<Type, object>();
        }

        private readonly BestFlightPriceContext _context;
        private readonly Dictionary<Type, Func<BestFlightPriceContext, object>> _repositoryFactories;
        private Dictionary<Type, object> _repositories;

        public T GetRepository<T>() where T : class
        {
            var targetType = typeof(T);
            if (_repositories.ContainsKey(targetType))
            {
                return (T)_repositories[targetType];
            }
            if (_repositoryFactories.ContainsKey(targetType))
            {
                var repository = (T)_repositoryFactories[targetType](_context);
                _repositories.Add(targetType, (object)repository);
                return repository;
            }
            throw new NotImplementedException("No factory for repository type, " + typeof(T).FullName);
        }
        public void Commit()
        {
            _context.SaveChanges();
        }
        public void Rollback()
        {
            _context
                .ChangeTracker
                .Entries()
                .ToList()
                .ForEach(x => x.Reload());
        }
        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
        }
    }
}
