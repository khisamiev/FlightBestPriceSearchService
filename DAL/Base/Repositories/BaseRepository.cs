﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Base.Models;
using System.Data.Entity;

namespace DAL.Base.Repositories
{
    public abstract class BaseRepository<Entity, TKey> : IRepository<Entity, TKey>, IDisposable
        where TKey : struct
        where Entity : class, IBaseEntity
    {
        protected DbContext dbContext;
        protected DbSet<Entity> dbSet;

        public BaseRepository(DbContext context)
        {
            dbContext = context;
            dbSet = context.Set<Entity>();
        }

        #region Члены IRepository<Entity>

        public async virtual Task<List<Entity>> GetAll()
        {
            return await dbSet.ToListAsync();
        }

        public virtual Entity Insert(Entity entity)
        {
            return dbSet.Add(entity);
        }

        public virtual void Update(Entity entity)
        {
            dbContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(Entity entity)
        {
            dbSet.Remove(entity);
        }

        public abstract Task<Entity> Find(TKey id);
        public abstract void Delete(TKey id);

        #endregion

        #region Члены IDisposable

        public void Dispose()
        {
            if (dbContext != null)
            {
                dbSet = null;
                dbContext.Dispose();
            }
        }

        #endregion
    }
}
