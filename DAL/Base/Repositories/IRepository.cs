﻿using DAL.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Base.Repositories
{
    /// <summary>
    /// Интерфейс базового репозитория.
    /// </summary>
    /// <typeparam name="Entity"></typeparam>
    public interface IRepository<Entity, TKey> : IDisposable
        where Entity : IBaseEntity
        where TKey : struct
    {
        /// <summary>
        /// Свойство для получения всех объектов.
        /// </summary>
        /// <value>
        /// Все объекты.
        /// </value>
        Task<List<Entity>> GetAll();
        /// <summary>
        /// Поиск объекта с указанным идентификатором.
        /// </summary>
        /// <param name="id">Идентификатор.</param>
        /// <returns>Объект.</returns>
        Task<Entity> Find(TKey id);
        /// <summary>
        /// Создание объекта в БД.
        /// </summary>
        /// <param name="entity">Объект.</param>
        /// <returns>Объект.</returns>
        Entity Insert(Entity entity);
        /// <summary>
        /// Обновление объекта в БД.
        /// </summary>
        /// <param name="entity">Объект.</param>
        void Update(Entity entity);
        /// <summary>
        /// Удаление объекта из БД
        /// </summary>
        /// <param name="entity">Объект.</param>
        void Delete(Entity entity);
        /// <summary>
        /// Удаление объекта из БД по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор.</param>
        void Delete(TKey id);
    }
}
