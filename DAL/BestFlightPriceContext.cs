﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    /// <summary>
    /// Контекст БД.
    /// </summary>
    public class BestFlightPriceContext : DbContext
    {
        public BestFlightPriceContext()
            : base("Name=BestFlightPriceContext")
        {

        }

        /// <summary>
        /// Таблица цен.
        /// </summary>
        public DbSet<Price> Prices { get; set; }
        /// <summary>
        /// Таблица городов.
        /// </summary>
        public DbSet<City> Citites { get; set; }
        /// <summary>
        /// Таблица стран.
        /// </summary>
        public DbSet<Country> Countries { get; set; }
        /// <summary>
        /// Таблица валют.
        /// </summary>
        public DbSet<Currency> Currencies { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}
