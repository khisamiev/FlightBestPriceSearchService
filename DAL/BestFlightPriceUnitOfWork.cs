﻿using DAL.Base;
using DAL.Repositories.Cities;
using DAL.Repositories.Countries;
using DAL.Repositories.Currencies;
using DAL.Repositories.Prices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    /// <summary>
    /// Объект доступа к данным BestFlightPriceContext.
    /// </summary>
    public class BestFlightPriceUnitOfWork : BaseUnitOfWork, IBestFlightPriceUnitOfWork
    {
        public BestFlightPriceUnitOfWork()
            : base()
        {
        }

        public BestFlightPriceUnitOfWork(BestFlightPriceContext context)
            : base(context)
        {
        }
        public ICityRepository Cities { get { return GetRepository<ICityRepository>(); } }
        public ICountryRepository Countries { get { return GetRepository<ICountryRepository>(); } }
        public ICurrencyRepository Currencies { get { return GetRepository<ICurrencyRepository>(); } }
        public IPriceRepository Prices { get { return GetRepository<IPriceRepository>(); } }        
    }
}
