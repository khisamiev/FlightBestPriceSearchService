﻿var service;

$(function () {
    service = new FlightPriceService();
    service.init();
});

///<Summary>
///  Сервис.
///</Summary>
function FlightPriceService() {
    $("#OriginId").prop("disabled", true);
    $("#PeriodType").prop("disabled", true);
    $("#ResidencePeriod").prop("disabled", true);
}

///<Summary>
/// Инициализация.
///</Summary>
FlightPriceService.prototype.init = function () {
    this.onLoadFormData();
}

///<Summary>
/// Инициализация.
///</Summary>
FlightPriceService.prototype.onLoadFormData = function () {
    var self = this;
    $.get("/Home/AdditionalData", { })
       .done(function (data) {
           if (data != undefined) {
               var object = data;
               if (object.Origins != undefined) {
                   self.setOrigins(object.Origins);
               }
               if (object.Types != undefined) {
                   self.setPeriodTypes(object.Types);
               }
               if (object.Periods != undefined) {
                   self.setResidencePeriods(object.Periods);
               }
           }
       });
}
///<Summary>
/// Установка пунктов.
///</Summary>
FlightPriceService.prototype.setOrigins = function (array) {
    setDropDownList(array, "OriginId");
    $("#OriginId").prop("disabled", false);
}
///<Summary>
/// Установка типов.
///</Summary>
FlightPriceService.prototype.setPeriodTypes = function (array) {
    setDropDownList(array, "ResidencePeriod");
    $("#ResidencePeriod").prop("disabled", false);
}
///<Summary>
/// Установка периодов.
///</Summary>
FlightPriceService.prototype.setResidencePeriods = function (array) {
    setDropDownList(array, "PeriodType");
    $("#PeriodType").prop("disabled", false);
}

///<Summary>
/// Инициализация.
///</Summary>
FlightPriceService.prototype.subsribeOnSearchClick = function () {
    $('a').click(function (e) {
        e.preventDefault();        
        var page = 1;
        try {
            page = (this.href.split('?')[1]).split('&')[1].split('=')[1];
        } catch (e) {
            page = (this.href.split('?')[1]).split('=')[1];
        }
        $.post("/Home/Search", {
            OriginId: $("#OriginId").val(),
            DepartDate: $("#DepartDate").val(),
            PeriodType: $("#PeriodType").val(),
            ResidencePeriod: $("#ResidencePeriod").val(),
            Page: page
        })
        .done(function (data) {
          if (data != undefined) {
              if (data.Table != undefined) {
                  $("#tableContainer").html(data.Table);
                  service.subsribeOnSearchClick();
              }
              if (data.Json != undefined) {
                  $("#jsonContainer").text(data.Json);
              }
          }
      });
        return false;
    });
}

///<Summary>
///  Calback-функция ajax-формы.
///</Summary>
function onSearchComplete(data) {
    if (data != undefined && data.responseJSON != undefined) {
        var object = data.responseJSON;
        if (object.Table != undefined) {
            $("#tableContainer").html(object.Table);
            service.subsribeOnSearchClick();
        }
        if (object.Json != undefined) {
            $("#jsonContainer").text(object.Json);
        }
    }
}
function onGridLoaded() {
    service.subsribeOnSearchClick();
}
/// <Summary>
//  Установить элементы в лист.
/// </Summary>
/// <param name="list">Коллекция элементов.</param>
/// <param name="ddlId">Идентификатор листа.</param>
function setDropDownList(list, ddlId) {
    $("#" + ddlId).empty();
    try {
        appendToDropDownList(list, ddlId);
    } catch (e) { };
}
/// <Summary>
//  Дополнить лист элементами.
/// </Summary>
/// <param name="list">Коллекция элементов.</param>
/// <param name="ddlId">Идентификатор листа.</param>
function appendToDropDownList(list, ddlId) {
    var optGroupName;
    var optgroup;
    var dropDownList = $('#' + ddlId);
    var isGrouped = false;
    //var indexOpt = 100;

    for (var index in list) {
        var item = list[index];

        var isGroup = item.Group != null;

        var groupName;
        if (isGroup)
            groupName = item.Group.Name;

        if (!isGrouped)
            isGrouped = isGroup;

        if (isGroup && groupName !== optGroupName) {
            optgroup = $('<optgroup/>', {
                label: groupName,
                Id: ddlId + "_" + item.Group.Id
            });

            dropDownList.append(optgroup);
            optGroupName = groupName;
        }

        var option = $('<option/>', {
            value: item.Value,
            text: item.Text,
            selected: item.Selected
        });

        if (isGroup) {
            optgroup.append(option);
        }
        else {
            dropDownList.append(option);
        }
    }

    if (isGrouped) {
        optgroup.append(optgroup);
    }
    else {
        dropDownList.append(optgroup);
    }
}