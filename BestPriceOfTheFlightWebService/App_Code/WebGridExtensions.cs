﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.WebPages;

namespace BestPriceOfTheFlightWebService.App_Code
{
    /// <summary>
    /// Расширение для WebGrid'а.
    /// </summary>
    public static class WebGridExtensions
    {
        /// <summary>
        /// Pager-для таблицы.
        /// </summary>
        /// <param name="webGrid"></param>
        /// <param name="mode"></param>
        /// <param name="firstText"></param>
        /// <param name="previousText"></param>
        /// <param name="nextText"></param>
        /// <param name="lastText"></param>
        /// <param name="numericLinksCount"></param>
        /// <param name="activePage"></param>
        /// <returns></returns>
        public static HelperResult PagerList(
            this WebGrid webGrid,
            WebGridPagerModes mode = WebGridPagerModes.NextPrevious | WebGridPagerModes.Numeric,
            string firstText = null,
            string previousText = null,
            string nextText = null,
            string lastText = null,
            int numericLinksCount = 5,
            Int32? activePage = null)
        {
            return PagerList(webGrid, mode, firstText, previousText, nextText, lastText, numericLinksCount, true, activePage);
        }

        private static HelperResult PagerList(
            WebGrid webGrid,
            WebGridPagerModes mode,
            string firstText,
            string previousText,
            string nextText,
            string lastText,
            int numericLinksCount,
            bool explicitlyCalled,
            Int32? activePage = null)
        {

            int currentPage = activePage.HasValue ? activePage.Value - 1 : webGrid.PageIndex;
            int totalPages = webGrid.PageCount;

            //int lastPage = totalPages - 1;
            int lastPage = (totalPages - 1) < 0 ? 0 : (totalPages - 1);

            var ul = new TagBuilder("ul");

            ul.MergeAttribute("class", "pagination", replaceExisting: false);

            var li = new List<TagBuilder>();

            if (ModeEnabled(mode, WebGridPagerModes.FirstLast))
            {
                if (String.IsNullOrEmpty(firstText))
                {
                    // firstText = "First";
                    firstText = "Первый";
                }

                var part = new TagBuilder("li")
                {
                    //InnerHtml = GridLink(webGrid, webGrid.GetPageUrl(0), firstText)
                    InnerHtml = GridLink(webGrid, webGrid.GetPageUrl(0), PageSpanTypes.First)
                };

                if (currentPage == 0)
                {
                    part.MergeAttribute("class", "disabled");
                }

                li.Add(part);

            }

            if (ModeEnabled(mode, WebGridPagerModes.NextPrevious))
            {
                if (String.IsNullOrEmpty(previousText))
                {
                    //previousText = "Prev";
                    previousText = "Предыдущий";
                }

                int page = currentPage == 0 ? 0 : currentPage - 1;

                var part = new TagBuilder("li")
                {
                    //InnerHtml = GridLink(webGrid, webGrid.GetPageUrl(page), previousText)
                    //InnerHtml = GridLink(webGrid, webGrid.GetPageUrl(page), previousText, "grid-prev-page")
                    InnerHtml = GridLink(webGrid, webGrid.GetPageUrl(page), PageSpanTypes.Prev)
                };

                if (currentPage == 0)
                {
                    part.MergeAttribute("class", "disabled");
                }

                li.Add(part);

            }


            if (ModeEnabled(mode, WebGridPagerModes.Numeric) && (totalPages > 1))
            {
                int last = currentPage + (numericLinksCount / 2);
                int first = last - numericLinksCount + 1;
                if (last > lastPage)
                {
                    first -= last - lastPage;
                    last = lastPage;
                }
                if (first < 0)
                {
                    last = Math.Min(last + (0 - first), lastPage);
                    first = 0;
                }
                for (int i = first; i <= last; i++)
                {

                    var pageText = (i + 1).ToString(CultureInfo.InvariantCulture);
                    var part = new TagBuilder("li")
                    {
                        // InnerHtml = GridLink(webGrid, webGrid.GetPageUrl(i), pageText)
                        InnerHtml = GridLink(webGrid, webGrid.GetPageUrl(i), pageText, "grid-page-link")
                    };

                    if (i == currentPage)
                    {
                        part.MergeAttribute("class", "active");
                    }

                    li.Add(part);

                }
            }

            if (ModeEnabled(mode, WebGridPagerModes.NextPrevious))
            {
                if (String.IsNullOrEmpty(nextText))
                {
                    // nextText = "Next";
                    nextText = "Следующий";
                }

                int page = currentPage == lastPage ? lastPage : currentPage + 1;

                var part = new TagBuilder("li")
                {
                    //InnerHtml = GridLink(webGrid, webGrid.GetPageUrl(page), nextText)
                    //InnerHtml = GridLink(webGrid, webGrid.GetPageUrl(page), nextText, "grid-next-page")
                    InnerHtml = GridLink(webGrid, webGrid.GetPageUrl(page), PageSpanTypes.Next)
                };

                if (currentPage == lastPage)
                {
                    part.MergeAttribute("class", "disabled");
                }

                li.Add(part);

            }

            if (ModeEnabled(mode, WebGridPagerModes.FirstLast))
            {
                if (String.IsNullOrEmpty(lastText))
                {
                    //  lastText = "Last";
                    lastText = "Крайний";
                }

                var part = new TagBuilder("li")
                {
                    //InnerHtml = GridLink(webGrid, webGrid.GetPageUrl(lastPage), lastText)
                    InnerHtml = GridLink(webGrid, webGrid.GetPageUrl(lastPage), PageSpanTypes.Last)
                };

                if (currentPage == lastPage)
                {
                    part.MergeAttribute("class", "disabled");
                }

                li.Add(part);

            }

            ul.InnerHtml = string.Join("", li);

            var html = "";
            if (explicitlyCalled && webGrid.IsAjaxEnabled)
            {
                var span = new TagBuilder("span");
                span.MergeAttribute("data-swhgajax", "true");
                span.MergeAttribute("data-swhgcontainer", webGrid.AjaxUpdateContainerId);
                span.MergeAttribute("data-swhgcallback", webGrid.AjaxUpdateCallback);

                span.InnerHtml = ul.ToString();
                html = span.ToString();

            }
            else
            {
                html = ul.ToString();
            }

            return new HelperResult(writer =>
            {
                writer.Write(html);
            });
        }

        private static String GridLink(WebGrid webGrid, string url, string text, string className)
        {
            TagBuilder builder = new TagBuilder("a");
            builder.SetInnerText(text);
            builder.MergeAttribute("href", url);
            builder.MergeAttribute("class", className, replaceExisting: false);
            if (webGrid.IsAjaxEnabled)
            {
                builder.MergeAttribute("data-swhglnk", "true");
                //builder.MergeAttribute("data-ajax", "true");
            }
            return builder.ToString(TagRenderMode.Normal);
        }

        private static String GridLink(WebGrid webGrid, string url, string text)
        {
            TagBuilder builder = new TagBuilder("a");
            builder.SetInnerText(text);
            builder.MergeAttribute("href", url);
            if (webGrid.IsAjaxEnabled)
            {
                builder.MergeAttribute("data-swhglnk", "true");
            }
            return builder.ToString(TagRenderMode.Normal);
        }


        private static String GridLink(WebGrid webGrid, string url, PageSpanTypes type)
        {
            TagBuilder builder = new TagBuilder("a");
            builder.InnerHtml = GetActionSpan(type);
            builder.MergeAttribute("href", url);
            builder.MergeAttribute("class", "grid-prev-page", replaceExisting: false);
            if (webGrid.IsAjaxEnabled)
            {
                builder.MergeAttribute("data-swhglnk", "true");
            }
            return builder.ToString(TagRenderMode.Normal);
        }

        private static String GetActionSpan(PageSpanTypes type)
        {
            string glyphicon = GetGlyphIconByType(type);
            TagBuilder builder = new TagBuilder("span");
            builder.MergeAttribute("class", glyphicon);
            return builder.ToString(TagRenderMode.Normal);
        }

        private static bool ModeEnabled(WebGridPagerModes mode, WebGridPagerModes modeCheck)
        {
            return (mode & modeCheck) == modeCheck;
        }

        private enum PageSpanTypes { First, Prev, Next, Last }

        private static string GetGlyphIconByType(PageSpanTypes type)
        {
            switch (type)
            {
                case PageSpanTypes.First:
                    return "glyphicon glyphicon-fast-backward";
                case PageSpanTypes.Prev:
                    return "glyphicon glyphicon-step-backward";
                case PageSpanTypes.Next:
                    return "glyphicon glyphicon-step-forward";
                case PageSpanTypes.Last:
                    return "glyphicon glyphicon-fast-forward";
                default:
                    return "";
            }
        }
    }
}