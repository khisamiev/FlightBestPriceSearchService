﻿using BestPriceOfTheFlightWebService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BestPriceOfTheFlightWebService.Contracts
{
    /// <summary>
    /// Контракт данных таблицы.
    /// </summary>
    public class PriceTableModelsContract
    {        
        /// <summary>
        /// Цены.
        /// </summary>
        public IEnumerable<PriceVM> Models { get; set; }
        /// <summary>
        /// Количество.
        /// </summary>
        public long Count { get; set; }
    }
}