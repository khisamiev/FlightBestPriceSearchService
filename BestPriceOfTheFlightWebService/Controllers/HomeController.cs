﻿using BestPriceOfTheFlightWebService.VIewHelper;
using BestPriceOfTheFlightWebService.ViewModels;
using BusinessLogic.Contracts;
using BusinessLogic.Logics.Interfaces;
using BusinessLogic.Models;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace BestPriceOfTheFlightWebService.Controllers
{
    /// <summary>
    /// Контроллер по умолчанию.
    /// </summary>
    public class HomeController : Controller
    {
        public HomeController(IPriceBusiness priceBusiness, ICityBusiness cityBusiness)
        {
            this._priceBusiness = priceBusiness;
            this._cityBusiness = cityBusiness;
        }

        private IPriceBusiness _priceBusiness;
        private ICityBusiness _cityBusiness;

        /// <summary>
        /// Начальная страница.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {            
            return View(new HomeIndexVM());
        }

        /// <summary>
        /// Загрузка данных формы. 
        /// </summary>
        /// <returns></returns>
        /// <remark>return  { Table = table, Json = json }</remark>
        [HttpGet]
        public async Task<ActionResult> AdditionalData()
        {
            var cities = await _cityBusiness.GetAll();
            var json = HomeViewHelper.GetAdditionalDataJson(cities);
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Данные таблицы.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        /// <remark> return { Origins, Types, Periods }</remark>
        [HttpPost]
        public async Task<ActionResult> Search(PriceSearchDTO dto) {
            var res = await _priceBusiness.Search(dto);
            var vm = HomeViewHelper.GetTableVM(res, dto);
            var json = HomeViewHelper.GetPriceResultJson(res);
            object jsonData = HomeViewHelper.GetTableJson(this, vm, json);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }
}
