﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BestPriceOfTheFlightWebService.VIewHelper
{
    /// <summary>
    /// Хэлпер визуализации.
    /// </summary>
    public class RazorViewHelper
    {
        #region Рендеринг в строку
        //-------------------------------------------------------------------
        /// <summary>
        /// Рендеринг view в строку.
        /// </summary>
        /// <param name="controller">Контроллер.</param>
        /// <param name="viewName">Путь до view.</param>
        /// <param name="model">Данные.</param>
        /// <returns></returns>
        public static string RenderToString(ControllerBase controller, string viewName, object model)
        {
            controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        //-------------------------------------------------------------------
        #endregion Рендеринг в строку
    }
}