﻿using BestPriceOfTheFlightWebService.Contracts;
using BestPriceOfTheFlightWebService.ViewModels;
using BusinessLogic.Contracts;
using BusinessLogic.Enums;
using BusinessLogic.Models;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BestPriceOfTheFlightWebService.VIewHelper
{
    /// <summary>
    /// Помощник подготовки данных для визуализации.
    /// </summary>
    public static class HomeViewHelper
    {
        /// <summary>
        /// Возвращает данные формы.
        /// </summary>
        public static object GetAdditionalDataJson(List<City> cities)
        {
            var origins = ToOrigins(cities);
            var types = ToPeriodType();
            var periods = ToResidencePeriod();
            return new { Origins = origins, Types = types, Periods = periods };
        }

        /// <summary>
        /// Возвращает данные поиска цен.
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="vm"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        public static object GetTableJson(ControllerBase controller, PriceTableVM vm, object obj)
        {
            var table = RazorViewHelper.RenderToString(controller, "~/Views/Home/Table.cshtml", vm);
            var json = new JavaScriptSerializer().Serialize(obj);
            return new { Table = table, Json = json };
        }

        /// <summary>
        /// Возвращает View-модель представления таблицы.
        /// </summary>
        /// <param name="dbModels">Модели.</param>
        /// <param name="dto">Контракт таблицы.</param>
        /// <returns></returns>
        public static PriceTableVM GetTableVM(PriceSearchModels dbModels, PriceSearchDTO dto)
        {
            return new PriceTableVM()
            {
                ModelsContract = new PriceTableModelsContract()
                {
                    Models = dbModels.Models != null ? dbModels.Models.Select(x => ToVM(x)).ToList() : null,
                    Count = dbModels.Count
                },
                PageSize = dto.PageSize,
                Page = dto.Page
            };
        }
        /// <summary>
        /// Возвращает Json-данные.
        /// </summary>
        /// <param name="dbModels"></param>
        /// <returns></returns>
        public static object GetPriceResultJson(PriceSearchModels dbModels)
        {
            return dbModels.Models.Select(x => new
            {
                destination = new
                {
                    id = x != null && x.Destination != null ? x.Destination.Id : default(Int16),
                    latitude = x != null && x.Destination != null ? x.Destination.Latitude : default(decimal),
                    longitude = x != null && x.Destination != null ? x.Destination.Longitude : default(decimal)
                },
                price = new
                {
                    value = x != null && x.Destination != null &&
                            x.Destination.Country != null && x.Destination.Country.BaseCurrency != null ?
                            x.Value * x.Destination.Country.BaseCurrency.ExchangeRate : default(decimal),
                    depart_date = x.DepartDate,
                    return_date = x.ReturnDate
                }
            }
            ).ToList();
        }

        #region Private methods
        //-------------------------------------------------------------------   
        /// <summary>
        /// Преобразование в модель представления.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private static PriceVM ToVM(Price obj)
        {
            if (obj == null)
                return null;
            return new PriceVM()
            {
                Destination = obj != null && obj.Destination != null ? obj.Destination.Name : String.Empty,
                Value = obj != null && obj.Destination != null &&
                obj.Destination.Country != null && obj.Destination.Country.BaseCurrency != null ?
                    obj.Value * obj.Destination.Country.BaseCurrency.ExchangeRate : default(decimal),
                DepartDate = obj != null ? obj.DepartDate : default(DateTime),
                ReturnDate = obj != null ? obj.ReturnDate : default(DateTime)
            };
        }

        private static List<SelectListItem> ToOrigins(List<City> cities) {
            if (cities == null)
                return new List<SelectListItem>();
            return cities.Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();
        }
        private static List<SelectListItem> ToPeriodType() {

            return new List<SelectListItem>(){
                new SelectListItem(){
                    Text = "Неделя",
                    Value = "0"
                },
                new SelectListItem(){
                    Text = "Месяц",
                    Value = "1"
                }
            };
        }
        private static List<SelectListItem> ToResidencePeriod() {

            return new List<SelectListItem>(){
                new SelectListItem(){
                    Text = "Не важно",
                    Value = "0"
                },
                new SelectListItem(){
                    Text = "1",
                    Value = "1"
                },
                new SelectListItem(){
                    Text = "2",
                    Value = "2"
                },
                new SelectListItem(){
                    Text = "3",
                    Value = "3"
                },
                new SelectListItem(){
                    Text = "4",
                    Value = "4"
                }
            };
        }
        //-------------------------------------------------------------------
        #endregion Private methods
    }
}