﻿using BestPriceOfTheFlightWebService.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BestPriceOfTheFlightWebService.ViewModels
{
    /// <summary>
    /// View-модель представления таблицы.
    /// </summary>
    public class PriceTableVM
    {
        /// <summary>
        /// Количество на странице.
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// Номер страницы.
        /// </summary>
        public int Page { get; set; }
        /// <summary>
        /// Контракт данных таблицы.
        /// </summary>
        public PriceTableModelsContract ModelsContract { get; set; }
    }
}