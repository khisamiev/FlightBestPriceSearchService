﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BestPriceOfTheFlightWebService.ViewModels
{
    /// <summary>
    /// View-модель представления цен.
    /// </summary>
    public class PriceVM
    {
        /// <summary>
        /// Пункт назначения.
        /// </summary>
        public String Destination { get; set; }
        /// <summary>
        /// Цена.
        /// </summary>
        public decimal Value { get; set; }
        /// <summary>
        /// Дата вылета.
        /// </summary>
        public DateTime DepartDate { get; set; }
        /// <summary>
        /// Дата обратного вылета.
        /// </summary>
        public DateTime ReturnDate { get; set; }
    }
}