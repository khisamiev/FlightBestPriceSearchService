﻿using BusinessLogic.Contracts;
using BusinessLogic.Enums;
using DAL.Contracts.Prices;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Helpers
{
    /// <summary>
    /// Хэлпер для поиска цен.
    /// </summary>
    public static class PriceSearchHelper
    {
        /// <summary>
        /// Вовзращает фильтр цен.
        /// </summary>
        /// <param name="dto">Контракт.</param>
        /// <returns></returns>
        public static IPriceFilterContract ToFilterContract(PriceSearchDTO dto)
        {
            return new PriceFilterContract() {
                Page = dto.Page,
                PageSize = dto.PageSize,
                OriginCityExpression = GetOriginCityExpression(dto.OriginId),
                DateRangeExpression = GetDateRangeExpression(dto.DepartDate, dto.PeriodType, dto.ResidencePeriod)
            };
        }

        #region Private methods
        //-------------------------------------------------------------------
        
        /// <summary>
        /// Выражение-фильтр по пункту отправления.
        /// </summary>
        /// <param name="originId"></param>
        /// <returns></returns>
        private static Expression<Func<Price, bool>> GetOriginCityExpression(Int16 originId)
        {
            return x => x.OriginId == originId;
        }
        /// <summary>
        /// Выражение-фильтр по времени.
        /// </summary>
        /// <param name="departDate"></param>
        /// <param name="periodType"></param>
        /// <param name="residencePeriod"></param>
        /// <returns></returns>
        private static Expression<Func<Price, bool>> GetDateRangeExpression(DateTime departDate, PeriodType periodType, ResidencePeriod residencePeriod)
        {
            var dayCount = GetDayCountByPeriod(periodType, residencePeriod);
            DateTime? returnDate = dayCount == default(int) ? (DateTime?)null : departDate.AddDays(dayCount);
            bool isReturnDateSetted = returnDate != null;
            return x => x.DepartDate >= departDate && (x.DepartDate < returnDate && isReturnDateSetted || !isReturnDateSetted);
        }
        /// <summary>
        /// Возвращает количество дней, которое соответствует периоду.
        /// </summary>
        /// <param name="periodType">Тип периода.</param>
        /// <param name="residencePeriod">Длительность пребывания.</param>
        private static int GetDayCountByPeriod(PeriodType periodType, ResidencePeriod residencePeriod){
            var typeDayCapacity = periodType == PeriodType.Week ? 7 : 31;
            return typeDayCapacity * (int)residencePeriod;
        }
        //-------------------------------------------------------------------
        #endregion Private methods

    }
}
