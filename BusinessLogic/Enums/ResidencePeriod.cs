﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Enums
{
    /// <summary>
    /// Длительность пребывания.
    /// </summary>
    public enum ResidencePeriod
    {
        /// <summary>
        /// Не важно.
        /// </summary>
        Zero = 0,
        /// <summary>
        /// Один.
        /// </summary>
        One = 1,
        /// <summary>
        /// Два.
        /// </summary>
        Two = 2,
        /// <summary>
        /// Три.
        /// </summary>
        Three = 3,
        /// <summary>
        /// Четяре.
        /// </summary>
        Four = 4
    }
}
