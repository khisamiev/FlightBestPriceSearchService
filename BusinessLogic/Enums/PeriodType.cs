﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Enums
{
    /// <summary>
    /// Тип периода.
    /// </summary>
    public enum PeriodType
    {
        /// <summary>
        /// Неделя.
        /// </summary>
        Week = 0,
        /// <summary>
        /// Месяц.
        /// </summary>
        Month = 1
    }
}
