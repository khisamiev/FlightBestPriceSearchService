﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Models
{
    /// <summary>
    /// Модель результата поиска.
    /// </summary>
    public class PriceSearchModels
    {        
        /// <summary>
        /// Цены.
        /// </summary>
        public IEnumerable<Price> Models { get; set; }
        /// <summary>
        /// Общее количество.
        /// </summary>
        public long Count { get; set; } 
    }
}
