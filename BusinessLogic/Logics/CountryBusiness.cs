﻿using BusinessLogic.Base;
using BusinessLogic.Logics.Interfaces;
using DAL;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Logics
{
    /// <summary>
    /// Бизнес-логика стран.
    /// </summary>
    public class CountryBusiness : BaseBusiness, ICountryBusiness
    {
        public CountryBusiness(IBestFlightPriceUnitOfWork uow)
            : base(uow)
        {

        }
        public void Add(Country contract)
        {
            _uow.Countries.Insert(contract);
            _uow.Commit();
        }
        public async Task<List<Country>> GetAll()
        {
            return await _uow.Countries.GetAll();  
        }
        public async Task<Country> Get(Int16 id)
        {
            return await _uow.Countries.Find(id);        
        }
        public async Task<bool> Delete(Int16 id)
        {
            try
            {
                _uow.Countries.Delete(id);
                _uow.Commit();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
