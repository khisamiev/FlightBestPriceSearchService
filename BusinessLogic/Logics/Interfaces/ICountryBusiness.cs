﻿using BusinessLogic.Base;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Logics.Interfaces
{
    /// <summary>
    /// Интерфейс бизнес-логики стран.
    /// </summary>
    public interface ICountryBusiness : IBaseBusiness
    {
        /// <summary>
        /// Добавление.
        /// </summary>
        /// <param name="deposit">Контракт.</param>
        void Add(Country contract);
        /// <summary>
        /// Возвращает все объекты.
        /// </summary>
        /// <returns></returns>
        Task<List<Country>> GetAll();
        /// <summary>
        /// Возвращает объект по идентификатору.
        /// </summary>
        /// <param name="filter">Идентификатор.</param>
        /// <returns></returns>
        Task<Country> Get(Int16 id);
        /// <summary>
        /// Удаление.
        /// </summary>
        /// <param name="id">Идентификатор.</param>
        /// <returns></returns>
        Task<bool> Delete(Int16 id);
    }
}
