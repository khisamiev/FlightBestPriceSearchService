﻿using BusinessLogic.Base;
using BusinessLogic.Contracts;
using BusinessLogic.Models;
using DAL.Contracts.Prices;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Logics.Interfaces
{
    /// <summary>
    /// Интерфейс бизнес-логики цен.
    /// </summary>
    public interface IPriceBusiness : IBaseBusiness
    {
        /// <summary>
        /// Добавление.
        /// </summary>
        /// <param name="deposit">Контракт.</param>
        void Add(Price contract);
        /// <summary>
        /// Возвращает все объекты.
        /// </summary>
        /// <returns></returns>
        Task<List<Price>> GetAll();
        /// <summary>
        /// Возвращает по ключу.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Price> Get(PriceCompositeKey id);
        /// <summary>
        /// Удаляет по колючу.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> Delete(PriceCompositeKey id);
        /// <summary>
        /// Поиск цен.
        /// </summary>
        /// <param name="dto">Контракт.</param>
        /// <returns></returns>
        Task<PriceSearchModels> Search(PriceSearchDTO dto);
    }
}
