﻿using BusinessLogic.Base;
using BusinessLogic.Logics.Interfaces;
using DAL;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Logics
{
    /// <summary>
    /// Бизнес-логика валют.
    /// </summary>
    public class CurrencyBusiness : BaseBusiness, ICurrencyBusiness
    {
        public CurrencyBusiness(IBestFlightPriceUnitOfWork uow)
            : base(uow)
        {

        }
        public void Add(Currency contract) {
            _uow.Currencies.Insert(contract);
            _uow.Commit();
        }
        public async Task<List<Currency>> GetAll() {
            return await _uow.Currencies.GetAll();  
        }
        public async Task<Currency> Get(Int16 id)
        {
            return await _uow.Currencies.Find(id);             
        }
        public async Task<bool> Delete(Int16 id)
        {
            try
            {
                _uow.Currencies.Delete(id);
                _uow.Commit();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
