﻿using BusinessLogic.Base;
using BusinessLogic.Contracts;
using BusinessLogic.Helpers;
using BusinessLogic.Logics.Interfaces;
using BusinessLogic.Models;
using DAL;
using DAL.Contracts.Prices;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Logics
{
    /// <summary>
    /// Бизнес-логика цен.
    /// </summary>
    public class PriceBusiness : BaseBusiness, IPriceBusiness
    {
        public PriceBusiness(IBestFlightPriceUnitOfWork uow)
            : base(uow)
        {

        }
        public void Add(Price contract)
        {
            _uow.Prices.Insert(contract);
            _uow.Commit();
        }
        public async Task<List<Price>> GetAll()
        {
            return await _uow.Prices.GetAll();
        }

        public async Task<Price> Get(PriceCompositeKey id)
        {
            return await _uow.Prices.Find(id);
        }
        public async Task<bool> Delete(PriceCompositeKey id)
        {
            try
            {
                _uow.Prices.Delete(id);
                _uow.Commit();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<PriceSearchModels> Search(PriceSearchDTO dto)
        {
            var filter = PriceSearchHelper.ToFilterContract(dto);
            var models = await _uow.Prices.Get(filter);
            var count = await _uow.Prices.GetCount(filter);
            return new PriceSearchModels()
            {
                Models = models,
                Count = count
            };
        }
    }
}
