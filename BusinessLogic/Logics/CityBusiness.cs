﻿using BusinessLogic.Base;
using BusinessLogic.Logics.Interfaces;
using DAL;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Logics
{
    /// <summary>
    /// Бизнес-логика городов.
    /// </summary>
    public class CityBusiness : BaseBusiness, ICityBusiness
    {
        public CityBusiness(IBestFlightPriceUnitOfWork uow)
            : base(uow)
        {

        }
        public void Add(City contract) {
            _uow.Cities.Insert(contract);
            _uow.Commit();
        }
        public async Task<List<City>> GetAll()
        {
            return await _uow.Cities.GetAll();       
        }
        public async Task<City> Get(Int16 id)
        {
            return await _uow.Cities.Find(id);        
        }
        public async Task<bool> Delete(Int16 id)
        {
            try
            {
                _uow.Cities.Delete(id);
                _uow.Commit();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
