﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Base
{
    /// <summary>
    /// Базовая бизнес-локика.
    /// </summary>
    public class BaseBusiness : IBaseBusiness
    {
        public BaseBusiness(IBestFlightPriceUnitOfWork uow)
        {
            this._uow = uow;
        }

        protected readonly IBestFlightPriceUnitOfWork _uow;
    }
}
