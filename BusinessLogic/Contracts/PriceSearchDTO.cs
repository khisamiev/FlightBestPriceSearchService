﻿using BusinessLogic.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Contracts
{
    /// <summary>
    /// Контракт для поиска.
    /// </summary>
    public class PriceSearchDTO
    {        
        /// <summary>
        /// Город вылета.
        /// </summary>
        public Int16 OriginId { get; set; }
        /// <summary>
        /// Дата вылета.
        /// </summary>
        public DateTime DepartDate { get; set; }
        /// <summary>
        /// Тип периода.
        /// </summary>
        public PeriodType PeriodType { get; set; }
        /// <summary>
        /// Длительность пребывания.
        /// </summary>
        public ResidencePeriod ResidencePeriod { get; set; }
        // <summary>
        /// Размер страницы.
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// Страница.
        /// </summary>
        public int Page { get; set; }

        public PriceSearchDTO()
        {
            Page = 1;
            PageSize = 20;
        }
    }
}
